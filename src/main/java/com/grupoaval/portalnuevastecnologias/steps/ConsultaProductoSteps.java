package com.grupoaval.portalnuevastecnologias.steps;


import com.grupoaval.portalnuevastecnologias.pageobjects.ConsultaProductoPage;

import net.thucydides.core.annotations.Step;

public class ConsultaProductoSteps {

	ConsultaProductoPage consultaProducto= new ConsultaProductoPage();
	
	@Step
	public void clickEnCatergoriaMenuSuperior(String categoria) {
		consultaProducto.clicEnBotonDeCategoriaMenuSuperior(categoria);
	}
	
	@Step
	public void validarProductoEnCategoriaSeleccionada(String nombreProducto) {
		consultaProducto.validarExistenciaProductoCategoriaSeleccioanda(nombreProducto);
	}
	
	@Step
	public void seleccionarComprarIrAProductos() {
		consultaProducto.clickComprarIrAProductos();
	}
	
	@Step
	public void seleccioneCategoriaMenuIzquierdo(String categoria) {
		consultaProducto.clicEnBotonDeCategoriaMenuIzquierdo(categoria);
	}
	
	@Step
	public void seleccioneCategoriaMenuInferiorInicial(String categoria) {
		consultaProducto.clicEnBotonDeCategoriaMenuInferiorInicial(categoria);
	}
	
	@Step
	public void seleccionaValorPrecioDesde(Integer precioDesde) {
		consultaProducto.seleccionarValorPrecioDesde(precioDesde);
	}
	
	@Step
	public void seleccionaValorPrecioHasta(Integer precioHasta) {
		consultaProducto.seleccionarValorPrecioHasta(precioHasta);
	}
	
	@Step
	public void clickButtonConsultarPrecio() {
		consultaProducto.clickButtonConsultarPrecio();
	}
	
	@Step
	public void seDirigeAOpcionDisponiblidadYLaSelecciona(String disponibilidad) {
		consultaProducto.seDirigeAOpcionDisponiblidadYLaSelecciona(disponibilidad);
	}
	
	@Step
	public void clickButtonConsultarDisponibilidad() {
		consultaProducto.clickButtonConsultarDisponibilidad();
	}
	
	@Step
	public void clickButtonVistaRapidaPrimerProducto() {
		consultaProducto.clickButtonVistaRapidaPrimerProducto();
	}
	
	@Step
	public void validarNombrePrimerProductoVistaRapida(String nombrePrimerProducto) {
		consultaProducto.validarNombrePrimerProductoVistaRapida(nombrePrimerProducto);
	}
	
	@Step
	public void clickButtonViewFullDetails() {
		consultaProducto.clickButtonViewFullDetails();
	}
	
	@Step
	public void clickButtonCerrarVistaRapida() {
		consultaProducto.clickButtonCerrarVistaRapida();
	}

	
}
