package com.grupoaval.portalnuevastecnologias.stepdefinitions;

import java.util.List;

import com.grupoaval.portalnuevastecnologias.entities.Carrito;
import com.grupoaval.portalnuevastecnologias.steps.ConsultaProductoSteps;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;

public class ConsultaProductosStepDefinitions {

	@Steps
	ConsultaProductoSteps consultaProductoSteps;
	
	String categoriaMenuSuperior;
	String cateorgiaMenuIzquierdo;
	String cateorgiaMenuInferior;

	// Escenario 1
	
	@Cuando("^selecciona una categoria en el menú superior de la pagina$")
	public void seleccioneCategoriaMenuSuperior(List<Carrito> listCarrito) {
		categoriaMenuSuperior = listCarrito.get(0).getCategoria();
		consultaProductoSteps.clickEnCatergoriaMenuSuperior(categoriaMenuSuperior);
	}
	
	@Entonces("^valida que aparezca el producto relacionado:$")
	public void validarProductoEnCategoriaSeleccionada(List<String> nombreProducto) {
		consultaProductoSteps.validarProductoEnCategoriaSeleccionada(nombreProducto.get(0));
	}

	// Escenario 2
	
	@Cuando("^ingresa a productos$")
	public void seleccioneCategoriaMenuSuperior() {
		consultaProductoSteps.seleccionarComprarIrAProductos();
	}
	
	@Cuando("^selecciona una categoria en el menu del lado izquierdo de la pagina:$")
	public void seleccioneCategoriaMenuIzquierdo(List<Carrito> listCarrito) {
		cateorgiaMenuIzquierdo = listCarrito.get(0).getCategoria();
		consultaProductoSteps.seleccioneCategoriaMenuIzquierdo(cateorgiaMenuIzquierdo);
	}
	
	// Escenario 3
	
	@Cuando("^selecciona una categoria en el menu inferior de la pagina inicial:$")
	public void seleccioneCategoriaMenuInferiorInicial(List<Carrito> listCarrito) {
		cateorgiaMenuInferior = listCarrito.get(0).getCategoria();
		consultaProductoSteps.seleccioneCategoriaMenuInferiorInicial(cateorgiaMenuInferior);
	}
	
	// Escenario 4
	
	@Cuando("^Se elige un valor para el campo precio desde:$")
	public void seleccionaValorPrecioDesde(List<Integer> precioDesde) {
		consultaProductoSteps.seleccionaValorPrecioDesde(precioDesde.get(0));
	}
	
	@Cuando("^Se elige un valor para el campo precio hasta:$")
	public void seleccionaValorPrecioHasta(List<Integer> precioHasta) {
		consultaProductoSteps.seleccionaValorPrecioHasta(precioHasta.get(0));
	}
	
	@Cuando("^presiona boton consultar precio$")
	public void clickButtonConsultarPrecio() {
		consultaProductoSteps.clickButtonConsultarPrecio();
	}
	
	// Escenario 5
	
	@Cuando("^se dirige a la opcion disponibilidad y selecciona:$")
	public void seDirigeAOpcionDisponiblidadYLaSelecciona(List<String> disponibilidad) {
		consultaProductoSteps.seDirigeAOpcionDisponiblidadYLaSelecciona(disponibilidad.get(0));
	}
	
	@Cuando("^presiona boton consultar disponibilidad$")
	public void clickButtonConsultarDisponibilidad() {
		consultaProductoSteps.clickButtonConsultarDisponibilidad();
	}
	
	// Escenario 7
	
	@Cuando("^preciona el icono vista rapida del primer producto$")
	public void clickButtonVistaRapidaPrimerProducto() {
		consultaProductoSteps.clickButtonVistaRapidaPrimerProducto();
	}
	
	@Entonces("^valida que aparezcan el nombre del primer producto:$")
	public void validarNombrePrimerProductoVistaRapida(List<String> nombreProducto) {
		consultaProductoSteps.validarNombrePrimerProductoVistaRapida(nombreProducto.get(0));
	}
	
	// Escenario 8
	@Cuando("^presiona la opcion View full details$")
	public void clickButtonViewFullDetails() {
		consultaProductoSteps.clickButtonViewFullDetails();
	}
	
	// Escenario 9
	@Cuando("^cierra la vista rapida$")
	public void clickButtonCerrarVistaRapida() {
		consultaProductoSteps.clickButtonCerrarVistaRapida();
	}
	
	

}
