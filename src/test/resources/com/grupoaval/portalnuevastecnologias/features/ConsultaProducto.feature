#language: es
Característica: Vizualizar Detalles de Los Productos

  Antecedentes: 
    Dado que Usuario ingresa al portal nuevas tecnologias
    Cuando ingresa su usuario y clave
      | Usuario | Clave      |
      | oswaldo | 1233903960 |
    Y presiona la tecla Enter

  @SmokeTest
  Escenario: Filtrar productos por categoria en el menú superior
    Cuando selecciona una categoria en el menú superior de la pagina
      | celulares |
    Entonces valida que aparezca el producto relacionado:
      | Parlante KALLEY Bluetooth Azul |

  @SmokeTest
  Escenario: Filtrar productos por categoria en el menú del lado izquierdo de la página
    Cuando ingresa a productos
    Y selecciona una categoria en el menu del lado izquierdo de la pagina:
      | celulares |
    Entonces valida que aparezca el producto relacionado:
      | Parlante KALLEY Bluetooth Azul |

  @SmokeTest
  Escenario: Filtrar productos por categoria Audio en el menú inferior
    Y selecciona una categoria en el menu inferior de la pagina inicial:
      | celulares |
    Entonces valida que aparezca el producto relacionado:
      | Parlante KALLEY Bluetooth Azul |

  @SmokeTest
  Escenario: Filtrar productos por precio
    Cuando ingresa a productos
    Y selecciona una categoria en el menu del lado izquierdo de la pagina:
      | celulares |
    Y Se elige un valor para el campo precio desde:
      | 0 |
    Y Se elige un valor para el campo precio hasta:
      | 100 |
    Y presiona boton consultar precio
    Entonces valida que aparezca el producto relacionado:
      | Parlante KALLEY Bluetooth Azul |

  @SmokeTest
  Escenario: Filtrar productos por la opción producto disponible
    Cuando ingresa a productos
    Y se dirige a la opcion disponibilidad y selecciona:
      | disponible |
    Y presiona boton consultar disponibilidad
    Entonces valida que aparezca el producto relacionado:
      | Parlante KALLEY Bluetooth Azul |

  @SmokeTest
  Escenario: Filtrar productos por la opción Todas las Categorías en el menú derecho
    Cuando ingresa a productos
    Y selecciona una categoria en el menu del lado izquierdo de la pagina:
      | todas las categorias |
    Entonces valida que aparezca el producto relacionado:
      | Parlante KALLEY Bluetooth Azul |

  @SmokeTest
  Escenario: Abrir vista rapida y verificar el nombre del producto
    Cuando ingresa a productos
    Y preciona el icono vista rapida del primer producto
    Entonces valida que aparezca el producto relacionado:
      | Audífonos Sony internos y Funcion Manos Libres- MDR-EX15AP |

  @SmokeTest
  Escenario: Abrir vista rapida y presionar la opción View full details
    Cuando ingresa a productos
    Y preciona el icono vista rapida del primer producto
    Y presiona la opcion View full details
    Entonces valida que aparezcan el nombre del primer producto:
      | Audífonos Sony internos y Funcion Manos Libres- MDR-EX15AP |

  @SmokeTest
  Escenario: Abrir vista rapida y cerrarla
    Cuando ingresa a productos
    Y preciona el icono vista rapida del primer producto
    Entonces valida que aparezcan el nombre del primer producto:
      | Audífonos Sony internos y Funcion Manos Libres- MDR-EX15AP |
    Y cierra la vista rapida
