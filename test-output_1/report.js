$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/com/grupoaval/portalnuevastecnologias/features/InicioDeSesion.feature");
formatter.feature({
  "name": "Inicio de sesion",
  "description": "  Como usuario registrado, quiero iniciar sesion en el portal nuevas tecnologias\n  para poder hacer compras.",
  "keyword": "Característica"
});
formatter.scenario({
  "name": "Inicio de sesion exitoso presionando Enter",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.queDavidIntreAlPortalAFC()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa su usuario y clave",
  "rows": [
    {
      "cells": [
        "Usuario",
        "Clave"
      ]
    },
    {
      "cells": [
        "oswaldo",
        "1233903960"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.ingresaSuUsuarioYClave(Usuario\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona la tecla Enter",
  "keyword": "Y "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.presionaLaTeclaEnter()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "deberia ver el siguiente mensaje en pantalla:",
  "rows": [
    {
      "cells": [
        "Login Exitoso"
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.deberiaVerElSiguienteMensajeEnPantalla(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Inicio de sesion exitoso presionando boton Ingresar",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.queDavidIntreAlPortalAFC()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa su usuario y clave",
  "rows": [
    {
      "cells": [
        "Usuario",
        "Clave"
      ]
    },
    {
      "cells": [
        "oswaldo",
        "1233903960"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.ingresaSuUsuarioYClave(Usuario\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona boton Ingresar",
  "keyword": "Y "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.presionaBotonIngresar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "deberia ver el siguiente mensaje en pantalla:",
  "rows": [
    {
      "cells": [
        "Login Exitoso"
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.deberiaVerElSiguienteMensajeEnPantalla(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Inicio de sesion validando nombre de usuario",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.queDavidIntreAlPortalAFC()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa su usuario y clave",
  "rows": [
    {
      "cells": [
        "Usuario",
        "Clave"
      ]
    },
    {
      "cells": [
        "oswaldo",
        "1233903960"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.ingresaSuUsuarioYClave(Usuario\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona la tecla Enter",
  "keyword": "Y "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.presionaLaTeclaEnter()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "deberia ver el siguiente mensaje en pantalla:",
  "rows": [
    {
      "cells": [
        "Login Exitoso"
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.deberiaVerElSiguienteMensajeEnPantalla(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "su nombre y usuario registrados deberian aparecer en el menu de Cuenta:",
  "rows": [
    {
      "cells": [
        "Oswaldo Buitrago"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.suNombreYUsuarioRegistradosDeberianAparecerEnElMenuDeCuenta(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Mensaje de Usuario Obligatorio",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
